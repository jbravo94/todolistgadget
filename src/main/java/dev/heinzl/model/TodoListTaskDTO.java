package dev.heinzl.model;

import dev.heinzl.entity.TodoListTask;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.xml.bind.annotation.*;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TodoListTaskDTO {

    @XmlElement
    private Integer id;

    @XmlElement
    private String description;

    @XmlElement
    private Boolean complete;

    public TodoListTaskDTO() {
    }

    public TodoListTaskDTO(TodoListTask todoListTask) {
        this.id = todoListTask.getID();
        this.description = todoListTask.getDescription();
        this.complete = todoListTask.isComplete();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getComplete() {
        return complete;
    }

    public void setComplete(Boolean complete) {
        this.complete = complete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TodoListTaskDTO that = (TodoListTaskDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(description, that.description) &&
                Objects.equals(complete, that.complete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, complete);
    }

    @Override
    public String toString() {
        return "TodoListTaskDTO{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", complete=" + complete +
                '}';
    }
}