package dev.heinzl.model;

import javax.xml.bind.annotation.*;
import java.util.List;
import java.util.Objects;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TodoListDTO {

    @XmlElement
    private List<TodoListTaskDTO> todoListTaskDTO;

    public TodoListDTO() {
    }

    public TodoListDTO(List<TodoListTaskDTO> todoListTaskDTO) {
        this.todoListTaskDTO = todoListTaskDTO;
    }

    public List<TodoListTaskDTO> getTodoListTaskDTO() {
        return todoListTaskDTO;
    }

    public void setTodoListTaskDTO(List<TodoListTaskDTO> todoListTaskDTO) {
        this.todoListTaskDTO = todoListTaskDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TodoListDTO that = (TodoListDTO) o;
        return Objects.equals(todoListTaskDTO, that.todoListTaskDTO);
    }

    @Override
    public int hashCode() {
        return Objects.hash(todoListTaskDTO);
    }

    @Override
    public String toString() {
        return "TodoListTaskDTO{" +
                "todoListTaskDTO=" + todoListTaskDTO +
                '}';
    }
}