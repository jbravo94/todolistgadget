package dev.heinzl.entity;

import net.java.ao.Entity;

public interface TodoListTask extends Entity
{
    String getDescription();

    void setDescription(String description);

    boolean isComplete();

    void setComplete(boolean isComplete);

    Long getUserId();

    void setUserId(Long userId);
}