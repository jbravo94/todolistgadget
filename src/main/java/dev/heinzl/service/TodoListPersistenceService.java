package dev.heinzl.service;

import com.atlassian.activeobjects.tx.Transactional;
import dev.heinzl.entity.TodoListTask;

import java.util.List;

@Transactional
public interface TodoListPersistenceService
{
    TodoListTask add(String description);

    List<TodoListTask> all();

    TodoListTask getTaskIdFromCurrentUser(int id);

    void removeTaskFromCurrentUserById(int id);

    void setCompletionStatusOfTaskIdFromCurrentUser(int taskId, boolean isComplete);
}