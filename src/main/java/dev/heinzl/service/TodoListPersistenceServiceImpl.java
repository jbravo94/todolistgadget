package dev.heinzl.service;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import dev.heinzl.entity.TodoListTask;
import net.java.ao.Query;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Arrays;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

@Scanned
@Named
public class TodoListPersistenceServiceImpl implements TodoListPersistenceService
{
    private final ActiveObjects ao;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    @Inject
    public TodoListPersistenceServiceImpl(@ComponentImport final ActiveObjects ao,
                                          @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext) {
        this.ao = checkNotNull(ao);
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    public TodoListTask add(String description) {
        final TodoListTask todoListTask = ao.create(TodoListTask.class);
        todoListTask.setDescription(description);
        todoListTask.setComplete(false);
        todoListTask.setUserId(getUserIdOfLoggedInUser());
        todoListTask.save();
        return todoListTask;
    }

    @Override
    public List<TodoListTask> all() {
        return Arrays.asList(ao.find(TodoListTask.class, Query.select().where("USER_ID = ?", getUserIdOfLoggedInUser())));
    }

    @Override
    public TodoListTask getTaskIdFromCurrentUser(int id) {
        List<TodoListTask> results = Arrays.asList(ao.find(TodoListTask.class, Query.select().where("ID = ? AND USER_ID = ?", id, getUserIdOfLoggedInUser())));
        if (results.size() == 1) {
            return results.get(0);
        }
        return null;
    }

    @Override
    public void removeTaskFromCurrentUserById(int id) {
        List<TodoListTask> results = Arrays.asList(ao.find(TodoListTask.class, Query.select().where("ID = ? AND USER_ID = ?", id, getUserIdOfLoggedInUser())));
        if (results.size() == 1) {
            ao.delete(results.get(0));
        }
    }

    @Override
    public void setCompletionStatusOfTaskIdFromCurrentUser(int taskId, boolean isComplete) {
        TodoListTask todoListTask = getTaskIdFromCurrentUser(taskId);
        if (todoListTask != null) {
            todoListTask.setComplete(isComplete);
            todoListTask.save();
        }
    }

    private Long getUserIdOfLoggedInUser() {
        return jiraAuthenticationContext.getLoggedInUser().getId();
    }
}