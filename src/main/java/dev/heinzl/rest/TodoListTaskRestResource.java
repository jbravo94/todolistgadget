package dev.heinzl.rest;

import dev.heinzl.model.TodoListDTO;
import dev.heinzl.model.TodoListTaskDTO;
import dev.heinzl.service.TodoListPersistenceService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("/")
public class TodoListTaskRestResource {

    private final TodoListPersistenceService todoListPersistenceService;

    public TodoListTaskRestResource(TodoListPersistenceService todoListPersistenceService) {
        this.todoListPersistenceService = todoListPersistenceService;
    }

    @GET
    @Path("/getmytodolisttasks")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllTodoListTasks() {

        List<TodoListTaskDTO> todoListTaskDTOS = todoListPersistenceService.all()
                .stream()
                .map(TodoListTaskDTO::new)
                .collect(Collectors.toList());

        return Response.ok(new TodoListDTO(todoListTaskDTOS)).build();
    }

    @PUT
    @Path("/updateTask")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateTask(TodoListTaskDTO todoListTaskDTO) {

        Integer taskId = todoListTaskDTO.getId();
        Boolean isComplete = todoListTaskDTO.getComplete();

        if (taskId == null || isComplete == null) {
            return Response.noContent().status(Response.Status.PRECONDITION_FAILED).build();
        }

        todoListPersistenceService.setCompletionStatusOfTaskIdFromCurrentUser(taskId, isComplete);
        return Response.ok().build();
    }

    @POST
    @Path("/createTask")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createTask(@QueryParam("description") String description) {

        return Response.ok(new TodoListTaskDTO(todoListPersistenceService.add(description))).build();
    }

    @DELETE
    @Path("/deleteTask")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteTask(@QueryParam("taskId") int taskId) {
        todoListPersistenceService.removeTaskFromCurrentUserById(taskId);
        return Response.ok().build();
    }
}