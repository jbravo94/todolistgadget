package dev.heinzl.crontasks.jobs;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.config.*;
import dev.heinzl.entity.TodoListTask;

public class ResetTodoListTasksJob {

    private static final String CRON_EXPRESSION = "0 0 1 ? * * *";
    //private static final String CRON_EXPRESSION = "/5 * * ? * * *";

    private final ActiveObjects activeObjects;

    public ResetTodoListTasksJob(ActiveObjects activeObjects) {
        this.activeObjects = activeObjects;
    }

    public JobRunner getJobRunner() {
        return jobRunnerRequest -> {
            return activeObjects.executeInTransaction(() -> {
                for (TodoListTask todoListTask : activeObjects.find(TodoListTask.class)) {
                    todoListTask.setComplete(false);
                    todoListTask.save();
                }
                activeObjects.flushAll();
                return JobRunnerResponse.success();
            });
        };
    }

    public JobConfig getJobConfig() {
        return JobConfig
                .forJobRunnerKey(JobRunnerKey.of(this.getClass().getCanonicalName() + "Scheduler"))
                .withSchedule(Schedule.forCronExpression(CRON_EXPRESSION))
                .withRunMode(RunMode.RUN_ONCE_PER_CLUSTER);
    }

    public JobId getJobId() {
        return JobId.of(this.getClass().getCanonicalName());
    }

    public JobRunnerKey getJobRunnerKey() {
        return getJobConfig().getJobRunnerKey();
    }
}
