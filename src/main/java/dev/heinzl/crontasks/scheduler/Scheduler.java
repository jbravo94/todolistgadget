package dev.heinzl.crontasks.scheduler;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.scheduler.SchedulerService;
import dev.heinzl.crontasks.jobs.ResetTodoListTasksJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;

@Scanned
@Named
public class Scheduler /*implements InitializingBean, DisposibleBean */ {

    private static final Logger LOG = LoggerFactory.getLogger(Scheduler.class);

    private final SchedulerService schedulerService;
    private final ResetTodoListTasksJob resetTodoListTasksJob;

    @Inject
    public Scheduler(@ComponentImport final SchedulerService schedulerService,
                     @ComponentImport final ActiveObjects activeObjects) {
        this.schedulerService = schedulerService;
        this.resetTodoListTasksJob = new ResetTodoListTasksJob(activeObjects);
        try {
            afterPropertiesSet();
        } catch (Exception e) {
            LOG.error("Error occured.", e);
        }
    }

    //@Override
    public void afterPropertiesSet() throws Exception {
        schedulerService.registerJobRunner(resetTodoListTasksJob.getJobRunnerKey(), resetTodoListTasksJob.getJobRunner());
        schedulerService.scheduleJob(resetTodoListTasksJob.getJobId(), resetTodoListTasksJob.getJobConfig());
    }

    /*@Override
    public void destroy() {
        schedulerService.unscheduleJob(resetTodoListTasksJob.getJobId());
        schedulerService.unregisterJobRunner(resetTodoListTasksJob.getJobRunnerKey());
    }*/
}
